import './App.css';
import {useState} from 'react';

function capitalize(string) {
  if (string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }
  return string;
}

function Rank(props) {
  const full = String.fromCharCode(9679);
  const empty = String.fromCharCode(9675);
  const rank = full.repeat(props.value) + empty.repeat(6 - props.value);
  return (<span>{rank}</span>);
}

function EditRank({set, value}) {
  const [edit, setEdit] = useState(false);
  const [temp, setTemp] = useState(value);
  if (edit) {
    const current = <input type="checkbox" name={temp} checked disabled />
    const filled = Array.from(Array(temp - 1), (_, index) =>
        <input key={index} name={index + 1} type="checkbox" checked onChange={e=>setTemp(+e.target.name)} />
    );
    const empty = Array.from(Array(6 - temp), (_, index) =>
        <input key={index + temp} name={index + 1 + temp} type="checkbox" onChange={e=>setTemp(+e.target.name)} />);

    return (
        <span>
            {filled} {current} {empty}
            <button onClick={()=>{setTemp(value); setEdit(false)}}>Cancel</button>
            <button onClick={()=>{set(temp); setEdit(false)}}>Save</button>
        </span>
    );
  }
  return (<span><Rank value={value}/><button onClick={()=>setEdit(true)}>Edit</button></span>);
} 

function Approaches({
  heart:[heart,setHeart],
  insight:[insight,setInsight],
  shadow:[shadow,setShadow]
}) {
  const setRank = (setter, value) => (rank) => setter({...value, rank})
  return (
      <div>
          <header><h2>Approaches</h2></header>
          <table>
            <tr><td/><th>Rank</th><th>Specialty</th><th>Flaw</th></tr>
            <tr>
              <td style={{'font-weight':'bold'}}>Heart</td>
              <td><EditRank name='Heart' value={heart.rank} set={setRank(setHeart, heart)} /></td>
              <td><Editable edit={!heart.specialty} save={(_,v)=>setHeart({...heart,specialty:v})}>{heart.specialty}</Editable></td>
              <td><Editable edit={!heart.flaw} save={(_,v)=>setHeart({...heart,flaw:v})}>{heart.flaw}</Editable></td></tr>
            <tr>
              <td style={{'font-weight':'bold'}}>Insight</td>
              <td><EditRank name='Insight' value={insight.rank} set={setRank(setInsight, insight)} /></td>
              <td><Editable edit={!insight.specialty} save={(_,v)=>setInsight({...insight,specialty:v})}>{insight.specialty}</Editable></td>
              <td><Editable edit={!insight.flaw} save={(_,v)=>setInsight({...insight,flaw:v})}>{insight.flaw}</Editable></td></tr>
            <tr>
              <td style={{'font-weight':'bold'}}>Shadow</td>
              <td><EditRank name='Shadow' value={shadow.rank} set={setRank(setShadow,shadow)} /></td>
              <td><Editable edit={!shadow.specialty} save={(_,v)=>setShadow({...shadow,specialty:v})}>{shadow.specialty}</Editable></td>
              <td><Editable edit={!shadow.flaw} save={(_,v)=>setShadow({...shadow,flaw:v})}>{shadow.flaw}</Editable></td></tr>
          </table>
      </div>
  );
}

function Edit(props) {
  // props.children is always singular now
  const [edit, setEdit] = useState(props.edit);
  const [temp, setTemp] = useState( props.children);
  return (<span> <label className={edit && props.title?"visible":"invisible"}>{capitalize(props.title)}: </label><span className="writable" contentEditable={edit} 
    onInput={e=>setTemp(e.target.textContent)} 
    onClick={()=>setEdit(true)} 
    onDoubleClick={()=>{props.save(props.title,temp);setEdit(false)}}
    onKeyPress={e=>{
      if (e.key==="Enter") { 
        e.preventDefault();
        props.save(props.title, temp);
        setEdit(false); 
        return false;
      } 
      if (e.key === "Escape") {
        setTemp(props.children);
        setEdit(false);
        return false;
      }
      return true;
    }}
    >
      {props.children}
    </span></span>);
}

function Editable(props) {
  if (Array.isArray(props.children)) {
    return props.children.map((v, i) => <Editable key={i} title={props.title} edit={props.edit} save={props.save}>{v}</Editable>);
  }
  if (typeof props.children === 'object') {
    const Tag = props.children.type;
    return <Tag className={props.children.props.className}><Editable save={props.save} edit={props.edit} title={props.children.props.title}>{props.children.props.children}</Editable></Tag>;
  }
  return (<Edit title={props.title} edit={props.edit} save={props.save}>{props.children}</Edit>);
}

function Inventory({
  stress:[stress,setStress],
  items: [items, setItems],
  traits:[traits, setTraits],
  heart
}){
  const update = (setter, array, index) => (title, replacement) =>
    setter([...array.map((v,i) => {
      if (i === index) {
        v[title] = replacement;
        return v;
      }
      return v;
    })]);
  const slots = 2 * heart + 6;
  const taken = items.reduce((sum, item) => sum + +item.slots, 0) +
      traits.length + +stress;
  const itemSlots = items.map((item, index)=>
    <li key={index}>
      <Editable save={update(setItems, items, index)} edit={!item.name}>
        <span title="name">{item.name}</span><span title="quality">{item.quality}</span><span title="slots">{item.slots}</span>
      </Editable> <button title="Drop" onClick={()=>{items.splice(index, 1);setItems([...items])}}>Drop</button></li>)
  const traitSlots = traits.map((trait, index)=>
    <li key={index}>
      <Editable save={update(setTraits, traits, index)} edit={!trait.name}>

      <span title="value">{trait.value}</span><span title="name">{trait.name}</span><span title="appliesTo">{trait.appliesTo}</span>
        </Editable> <button title="Remove" onClick={()=>{traits.splice(index, 1);setTraits([...traits])}}>Remove</button></li>)
  return (
      <div>
          <header class="centered"><h2>Inventory</h2> 
              Stress: <Editable save={(_,s)=>setStress(s)}>{stress}</Editable>
              | Slots {slots-taken}/{slots} Remaining
          </header>
          <div class="inventory">
              <div>
                  <header><h3>Items</h3></header>
                  <ul>
                      {itemSlots}
                  </ul>
                  {slots-taken>0&&<button onClick={()=>setItems([...items,{name:'', quality:1, slots:1}])}>Add</button>}
              </div>

              <div>
                  <header><h3>Conditions</h3></header>
                  <ul>
                      {traitSlots}
                  </ul>
                  {slots-taken>0&&<button onClick={()=>setTraits([...traits,{name:'', appliesTo:'Heart', value:'+'}])}>Add</button>}
              </div>
          </div>
      </div>
  )
}

function rollDice(approach, value) {
    if (value > 0) {
      const results = Array.from(Array(value), () => Math.ceil(Math.random() * 6));
      const max = Math.max(...results);
      const extras = Math.max(0, results.filter(r => r === max).length - 1);
      return { approach:approach + ": " + value, results, max, extras };
    }
    const results = [Math.ceil(Math.random() * 6), Math.ceil(Math.random() * 6)];
    const min = Math.min(...results);
    return {approach: approach + ": -2", results, max:min, extras:0};
  }

function App(props) {
  const [name, setName] = useState(props.name);
  const [description, setDescription] = useState(props.description);
  const heart = useState(props.approaches.Heart);
  const insight = useState(props.approaches.Insight);
  const shadow = useState(props.approaches.Shadow);
  const stress = useState(props.stress);
  const items = useState(props.items);
  const traits = useState(props.traits);
  const [chosen, setChosen] = useState("Heart");
  const [rolls, setRolls] = useState([]);
  const [difficulty, setDifficulty] = useState(0);
  const approaches = {Heart:heart[0], Insight:insight[0], Shadow:shadow[0]};
  const rollDisplay = rolls.map(({ approach, results, max, extras }, index) => {
    return (
      <div key={index}>
        {approach}
        <div>[{results.join(", ")}]</div>
        {max}: {extras}
      </div>
    );
  });
   
  return (
    <div>
      <div class="main">
        <header><h1>Gradient: Crimson Sky Character Sheet</h1></header>
        
        <Editable save={(_,n)=>setName(n)}><div className="character-name" title="name">{name}</div></Editable>
        <Editable save={(_,d)=>setDescription(d)}><div className="character-description" title="description">{description}</div></Editable>
        <Approaches {...{heart, insight, shadow}} />
        <Inventory {...{stress, items, traits, heart:heart[0].rank}} />
        
      </div>

      <aside class="panel">
        <header><h2>Roll Dice</h2></header>
        <label><b>Approach</b><br/>
          <select value={chosen} onChange={(e) => setChosen(e.target.value)}> 
            <option>Heart</option>
            <option>Insight</option>
            <option>Shadow</option>
          </select>
        </label>
        <br/>
        <label><b>Difficulty</b> <br/>
          Hard <input id="difficulty" type="range" min="-5" max="5" value={difficulty} onChange={e=>setDifficulty(+e.target.value)} /> Easy
          <br/>
        <output for="difficulty" style={{textAlign:'center', width: '85%', display:'block'}}>{difficulty}</output>
        </label>
        <br/>
        <button onClick={()=>{
          rolls.push(rollDice(chosen, approaches[chosen].rank + difficulty))
          setRolls([...rolls])
        }}>Roll</button>
        <div class="window">{rollDisplay}</div>
        <button onClick={() => setRolls([])}>Clear</button>
      </aside>
    </div>);
}

export default App;