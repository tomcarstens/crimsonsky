import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

const boblin = {
  name: 'Boblin',
  description: 'Swashbuckling Goblin Hero of Justice',
  approaches: {
    Heart: { rank: 3, specialty: '', flaw: ''},
    Insight: { rank: 1, specialty: 'Sense for Justice', flaw: 'Sucker for a Smile'},
    Shadow: { rank: 2, specialty: '', flaw: ''}
  },
  items: [
    {
      name:'Shortsword',
      quality: 2,
      slots: 1
    },
    {
      name: 'Firebolt Tome',
      quality: 3,
      slots: 1
    }
  ],
  traits: [
    {
      name: 'Flameheart Boost',
      appliesTo: 'Heart',
      value: '+'
    },
    {
      name: 'Sprained Ankle',
      appliesTo: 'Shadow',
      value: '-'
    }
  ],
  stress: 2
};

ReactDOM.render(
  <React.StrictMode>
    <App {...boblin} />
  </React.StrictMode>,
  document.getElementById('root')
);
